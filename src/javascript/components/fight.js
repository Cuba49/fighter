import { addListener } from 'process';
import { controls } from '../../constants/controls';

const Emitter = require("events");
let emitter = new Emitter();
let eventName = "win";
let keysPressed = {};
let firstFighterCanHit = true;
let secondFighterCanHit = true;
let firstFighterCanSuperHit = false;
let secondFighterCanSuperHit = false;
let firstFighter;
let secondFighter;
export async function fight(_firstFighter, _secondFighter) {
  firstFighter = _firstFighter;
  secondFighter = _secondFighter;
  return new Promise((resolve) => {
    emitter.on(eventName, function(winner){
      resolve(winner);
    });
    document.getElementById("right-fighter-indicator").style.width = '100%';
    document.getElementById("left-fighter-indicator").style.width = '100%';
    changeStateSuperBlock("left");
    changeStateSuperBlock("right");
    addListeners();
    //emitter.emit(eventName, secondFighter);
    // resolve the promise with the winner when fight is over
  });
}

export function getDamage(attacker, defender) {
  let damage= getHitPower(attacker) - getBlockPower(defender)
  return damage>0?damage:0;
  // return damage
}

export function getHitPower(fighter) {
  return fighter.attack * (Math.random() + 1);
  // return hit power
}

export function getBlockPower(fighter) {
  return fighter.defense * (Math.random() + 1);
  // return block power
}

function changeStateSuperBlock(indicatorSuperHit) {
  if (indicatorSuperHit == "left") {
    firstFighterCanSuperHit = false;
    document.getElementById("left-fighter-super-hit").style.display = "none";
    setTimeout(function () { firstFighterCanSuperHit = true; document.getElementById("left-fighter-super-hit").style.display = "block"; }, 10*1000);
  } else {
    secondFighterCanSuperHit = false;
    document.getElementById("right-fighter-super-hit").style.display = "none";
    setTimeout(function () { secondFighterCanSuperHit = true; document.getElementById("right-fighter-super-hit").style.display = "block"; }, 10*1000);
  }
}

function firstFighterHit() {
  firstFighterCanHit = false;
  let damage = 0;
  if (keysPressed[controls.PlayerTwoBlock]) {
    damage = getDamage(firstFighter, secondFighter);
  } else {
    damage = getHitPower(firstFighter);
  }
  reloadHealth("right-fighter-indicator", damage, firstFighter, secondFighter);
  setTimeout(function () { firstFighterCanHit = true; }, 500);
}

function secondFighterHit() {
  secondFighterCanHit = false;
  let damage = 0;
  if (keysPressed[controls.PlayerOneBlock]) {
    damage = getDamage(secondFighter,firstFighter);
  } else {
    damage = getHitPower(secondFighter);
  }
  reloadHealth("left-fighter-indicator", damage, secondFighter, firstFighter);
  setTimeout(function () { secondFighterCanHit = true; }, 500);
}

function firstFighterSuperHit() {
  changeStateSuperBlock("left");
  let damage = firstFighter.attack*2;
  reloadHealth("right-fighter-indicator", damage, firstFighter, secondFighter);
}

function secondFighterSuperHit() {
  changeStateSuperBlock("right");
  let damage = secondFighter.attack*2;
  reloadHealth("left-fighter-indicator", damage, secondFighter, firstFighter);
}

let listener = function (event) {
  keysPressed[event.code] = true;
  if (keysPressed[controls.PlayerOneAttack] && !keysPressed[controls.PlayerOneBlock] && firstFighterCanHit) {
    firstFighterHit();
  } else if (keysPressed[controls.PlayerTwoAttack] && !keysPressed[controls.PlayerTwoBlock] && secondFighterCanHit) {
    secondFighterHit();
  } else if (controls.PlayerOneCriticalHitCombination.every(button=>keysPressed[button])&& !keysPressed[controls.PlayerOneBlock]&&firstFighterCanSuperHit) {
    firstFighterSuperHit();
  } else if (controls.PlayerTwoCriticalHitCombination.every(button=>keysPressed[button])&& !keysPressed[controls.PlayerTwoBlock]&&secondFighterCanSuperHit) {
    secondFighterSuperHit();
  }
}

function addListeners() {
  document.addEventListener('keydown', listener);

  document.addEventListener('keyup', (event) => {
    delete keysPressed[event.code];
  });
}
function reloadHealth(indicator, damage, attacker, defender) {
  let width = document.getElementById(indicator).style.width.replace('%', '');
  let health = width - damage * 100 / defender.health;
  if (health < 20) document.getElementById(indicator).style.background = "#ff0000";
  health = health > 0 ? health : 0;
  document.getElementById(indicator).style.width = health + "%";
  if (health == 0) {
    document.removeEventListener('keydown', listener);
    emitter.emit(eventName, attacker);
  }
}
