import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {
  const fighterImg = createFighterImage(fighter);
  showModal({ title: `${fighter.name} is win!`, bodyElement: fighterImg, onClose: () => { location.reload(); }})
  // call showModal function 
}
