import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const way = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  const fighterPicture = createElement({
    tagName: 'div',
    className: `${way}`
  });
  if (!fighter) {
    fighterElement.append(fighterPicture)
    return fighterElement
  };
  const fighterImg = createFighterImage(fighter);
  const name = createElement({
      tagName: 'p',
      className: `fighter-preview___name`
  });
  name.innerHTML=`${fighter.name}`
  fighterElement.append(name)
  fighterPicture.append(fighterImg)
  fighterElement.append(fighterPicture)

  const params = createElement({
      tagName: 'dav',
      className: `fighter-preview___params`
  });
  const health = createParam(fighter, 'health')
  params.append(health)
  const defense = createParam(fighter, 'defense')
  params.append(defense)
  const attack = createParam(fighter, 'attack')
  params.append(attack)
  
  fighterElement.append(params)

  // todo: show fighter info (image, name, health, etc.)

  return fighterElement;
}

function createParam(fighter, name) {
  const param = createElement({
    tagName: 'div',
    className:`info-block ${name}`
  });
  const nameParam = createElement({
    tagName: 'p',
    className:`name`
  });
  nameParam.innerHTML = name[0].toUpperCase() + name.slice(1);
  param.append(nameParam);
  const valueParam = createElement({
    tagName: 'p',
    className:`value`
  });
  valueParam.innerHTML = fighter[name];
  param.append(valueParam);
  return param;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
